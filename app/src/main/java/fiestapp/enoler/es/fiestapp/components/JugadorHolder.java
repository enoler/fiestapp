package fiestapp.enoler.es.fiestapp.components;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import de.hdodenhof.circleimageview.CircleImageView;
import fiestapp.enoler.es.fiestapp.R;

/**
 * Created by Beltran on 9/2/17.
 */

public class JugadorHolder extends RecyclerView.ViewHolder {
    public TextView txtCelebName, txtCelebMovie;
    public ImageView profileImage;

    public JugadorHolder(View view) {
        super(view);
        txtCelebName = (TextView) view.findViewById(R.id.txtCelebName);
        profileImage = (ImageView) view.findViewById(R.id.profileImage);
    }
}

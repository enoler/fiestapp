package fiestapp.enoler.es.fiestapp.fragments.Dado;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.*;
import android.widget.EditText;
import android.widget.ImageView;

import fiestapp.enoler.es.fiestapp.R;
import fiestapp.enoler.es.fiestapp.adapters.JugadorAdapter;
import fiestapp.enoler.es.fiestapp.fragments.MenuPrincipalFragment;
import fiestapp.enoler.es.fiestapp.models.Jugador;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.Random;
import java.util.Set;

/**
 *  Fragment para añadir jugadores.
 */
public class JugadoresFragment extends Fragment
{
    private JugadorAdapter jugadorAdapter;
    private FloatingActionButton floating_add;
    private FloatingActionButton floating_prapida;
    private FloatingActionButton floating_jugar;
    private ImageView bt_back;
    private RecyclerView recyclerView;
    ArrayList<Jugador> jugadoresList;
    int[] fotos;
    public JugadoresFragment() { }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {

        View view = inflater.inflate(R.layout.fragment_dado_jugadores_list, container, false);

        fotos = new int[]{R.drawable.conejo, R.drawable.gato, R.drawable.gorila, R.drawable.koala, R.drawable.leon, R.drawable.perro, R.drawable.tigre, R.drawable.zorro};

        // Inicializar.
       jugadoresList = new ArrayList<>();
        floating_add = (FloatingActionButton)view.findViewById(R.id.floating_add);
        floating_prapida = (FloatingActionButton)view.findViewById(R.id.floating_prapida);
        floating_jugar = (FloatingActionButton)view.findViewById(R.id.bt_dado_jugadores);
        bt_back = (ImageView)view.findViewById(R.id.action_back);


        // Aplicar adapter.
        Context context = view.getContext();
        recyclerView = (RecyclerView)view.findViewById(R.id.rv_dado_jugadores);
        jugadorAdapter = new JugadorAdapter(jugadoresList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        recyclerView.setAdapter(jugadorAdapter);

        // Manejador de click.
        floating_jugar.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                Click_Jugar();
            }
        });
        floating_add.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                Click_Add();
                floating_jugar.setVisibility(View.VISIBLE);
            }
        });

        bt_back.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                Click(new MenuPrincipalFragment());
            }
        });

        // Manejadores de click.
        floating_prapida.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                Click(new DadoFragment());
            }
        });

        // Se muestra el menú con el botón de actualizar.
        setHasOptionsMenu(true);

        return view;
    }

    //region Funciones

    /**
     * Obtiene los señores del 3 iniciales aleatoriamente.
     */
   private void Calcular_Sr_3()
    {
        int num_jugadores = jugadoresList.size();
        long num_sr_3 = Math.round(num_jugadores * 0.33);

        Random r = new Random();
        Set<Integer> generated = new LinkedHashSet<>();

        // Se meten en un Set para que no se repitan.
        while(generated.size() < num_sr_3)
        {
            Integer next = r.nextInt(num_jugadores);
            generated.add(next);
        }

        // Se pone a true
        for(Integer i : generated)
        {
            jugadoresList.get(i).setSr_3(true);
        }
    }

    /**
     * Añade un nuevo jugador.
     */
    private void Click_Add()
    {
        // Crear builder.
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final View dialogView = View.inflate(getContext(), R.layout.dialog_dado_jugadores, null);
        builder.setView(dialogView);
        final EditText et = (EditText) dialogView.findViewById(R.id.et_nombre);

        builder.setTitle(getActivity().getResources().getString(R.string.add_jugadores))
                .setMessage(getActivity().getResources().getString(R.string.escribir_nombre))
                .setPositiveButton(getActivity().getResources().getString(R.string.aceptar), new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int id)
                    {
                        Crear_Jugador(et);
                    }
                })
                .setNegativeButton(getActivity().getResources().getString(R.string.cancelar), new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int id) { }
                });

        // Se crea el Dialog y se retorna.
        AlertDialog b = builder.create();
        b.show();
    }

    /**
     * Se abre un fragment con la partida.
     */
    private void Click_Jugar()
    {
        // Calcular señores del 3.
           Calcular_Sr_3();

        // Cambiar el fragment.
         DadoFragment fragment = DadoFragment.newInstance(jugadoresList);
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.rl_fragment, fragment);
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    /**
     * Se crea un nuevo jugador y se añade a la lista.
     */
    private void Crear_Jugador(EditText et)
    {

        Jugador jugador = new Jugador();
        String nombre = et.getText().toString();
        if(nombre.equals(""))
            nombre = getResources().getString(R.string.jugador) + " " + (jugadoresList.size() + 1);
        jugador.setName(nombre);
        Random aleatorio=new Random();
        int i=aleatorio.nextInt(fotos.length);
        jugador.setProfilePhotoLocation(fotos[i]);
        jugadoresList.add(jugador);
        floating_prapida.setVisibility(View.GONE);
        jugadorAdapter.notifyDataSetChanged();


    }

    //endregion.


    private void Click(Fragment fragment)
    {
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.remove(this);
        transaction.replace(R.id.rl_fragment, fragment);
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.addToBackStack(null);
        transaction.commit();
    }


}

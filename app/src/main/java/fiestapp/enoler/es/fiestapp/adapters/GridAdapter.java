package fiestapp.enoler.es.fiestapp.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import fiestapp.enoler.es.fiestapp.R;
import fiestapp.enoler.es.fiestapp.activities.JuegoActivity;
import fiestapp.enoler.es.fiestapp.components.JuegoHolder;
import fiestapp.enoler.es.fiestapp.helper.Constantes;
import fiestapp.enoler.es.fiestapp.models.ElementoGrid;

import static fiestapp.enoler.es.fiestapp.helper.Constantes.SR3;

/**
 * Clase que adapta los datos al gridview correspondiente.
 */
public class GridAdapter extends RecyclerView.Adapter<JuegoHolder>{

    private List<ElementoGrid> juegosList;
    private Context context;


    public GridAdapter(Context context,List<ElementoGrid> juegosList) {
        this.juegosList = juegosList;
this.context = context;
    }

    @Override
    public JuegoHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.grid_item, parent, false);

        return new JuegoHolder(itemView);
    }

    @Override
    public void onBindViewHolder(JuegoHolder holder, int position) {
        ElementoGrid item = juegosList.get(position);
        holder.txtNombreJuego.setText(item.getNombre());
        holder.juegoImage.setImageResource(item.getImagen());
        holder.container.setOnClickListener(onClickListener(position));

    }

    @Override
    public int getItemCount() {
        return juegosList.size();
    }
    private View.OnClickListener onClickListener(final int position) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, JuegoActivity.class);
                Click_Juego(i,position);
            }
        };
    }

    //Metodo click para seleccionar el juego

    public void Click_Juego(Intent i,int position)
    {
        ElementoGrid juego = juegosList.get(position);

        switch(juego.getNombre())
        {
            case SR3:
                i.putExtra(Constantes.FRAGMENT, SR3);
                break;
            case Constantes.YO_NUNCA:
                i.putExtra(Constantes.FRAGMENT, Constantes.YO_NUNCA);
                break;
        }
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); //Abrir en una nueva tarea.
        context.startActivity(i); // Se abre el intent.*/
    }

}
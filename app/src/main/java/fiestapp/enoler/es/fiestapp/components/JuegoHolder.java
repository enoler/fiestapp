package fiestapp.enoler.es.fiestapp.components;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import fiestapp.enoler.es.fiestapp.R;

public class JuegoHolder extends RecyclerView.ViewHolder{

    public TextView txtNombreJuego;
    public ImageView juegoImage;
    public View container;

    public JuegoHolder(View view) {
        super(view);
        txtNombreJuego = (TextView) view.findViewById(R.id.tv_juego);
        juegoImage = (ImageView) view.findViewById(R.id.iv_juego);
        container = view.findViewById(R.id.juegos_card_view);

    }

}
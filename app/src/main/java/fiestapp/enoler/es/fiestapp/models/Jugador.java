package fiestapp.enoler.es.fiestapp.models;

import android.os.Parcel;
import android.os.Parcelable;

public class Jugador implements Parcelable{

    private String name;
    private int profilePhotoLocation;
    private boolean sr_3;

    public Jugador() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getProfilePhotoLocation() {
        return profilePhotoLocation;
    }

    public void setProfilePhotoLocation(int profilePhotoLocation) {
        this.profilePhotoLocation = profilePhotoLocation;
    }
    public boolean isSr_3()
    {
        return sr_3;
    }

    public void setSr_3(boolean sr_3)
    {
        sr_3 = sr_3;
    }

    //region Parcelable

    private Jugador(Parcel in)
    {
        super();
        readFromParcel(in);
    }

    public static final Parcelable.Creator<Jugador> CREATOR = new Parcelable.Creator<Jugador>()
    {
        public Jugador createFromParcel(Parcel in)
        {
            return new Jugador(in);
        }

        public Jugador[] newArray(int size)
        {
            return new Jugador[size];
        }
    };

    private void readFromParcel(Parcel in)
    {

        name = in.readString();
        sr_3   = in.readByte() != 0;
    }

    public int describeContents()
    {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeString(name);
        dest.writeByte((byte) (sr_3 ? 1 : 0));
    }

}

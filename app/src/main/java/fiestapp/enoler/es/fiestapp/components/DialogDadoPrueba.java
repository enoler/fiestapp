package fiestapp.enoler.es.fiestapp.components;

import android.app.AlertDialog;
import android.app.Dialog;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import java.util.List;
import java.util.Random;

import fiestapp.enoler.es.fiestapp.R;
import fiestapp.enoler.es.fiestapp.helper.Constantes;

/**
 * Dialog que se abre cuando toca jugar al Yo Nunca, Lista o Norma.
 */
public class DialogDadoPrueba extends DialogFragment
{
    //region Variables

    private List<String> lPruebas;
    private String juego;
    private String prueba;
    private String norma;

    //endregion

    @Override
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        Inicializar(this.getArguments());

        prueba = Obtener_Prueba();

        // Texto para el botón positivo.
        String cambiar;
        if(juego.equals(getResources().getString(R.string.yo_nunca)))
            cambiar = getResources().getString(R.string.cambiar_otro); // Se pone "Otro".
        else
            cambiar = getResources().getString(R.string.cambiar_otra); // Se pone "Otra".

        // Crear builder.
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setTitle(juego)
                .setMessage(prueba)
                .setPositiveButton(cambiar, new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int id)
                    {
                        Cambiar_Prueba(builder, prueba);
                    }
                })
                .setNegativeButton(R.string.terminar, new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int id)
                    {
                        // Se cierra el dialog.
                        if (juego.equals(norma))
                        {
                            sendResult(0);
                        }
                        else
                            sendResult(1);
                    }
                });

        // Se crea el Dialog y se retorna.
        return builder.create();
    }

    //region Funciones

    /**
     * Busca una prueba diferente a la última mostrada.
     *
     * @param builder        Instancia del dialog.
     * @param prueba_antigua Prueba anterior para no repetirla.
     */
    private void Cambiar_Prueba(AlertDialog.Builder builder, String prueba_antigua)
    {
        String prueba_nueva;

        // Para que no se repita la anterior.
        do
        {
            prueba_nueva = Obtener_Prueba();
        }
        while (prueba_nueva.equals(prueba_antigua));

        prueba = prueba_nueva;

        builder.setMessage(prueba);
        builder.create();
        builder.show();
    }

    /**
     * Obtiene el nombre del juego y sus correspondientes pruebas.
     *
     * @param savedInstanceState Bundle de datos que contiene el nombre y el Arraylist con las pruebas.
     */
    private void Inicializar(Bundle savedInstanceState)
    {
        lPruebas = savedInstanceState.getStringArrayList(Constantes.PRUEBAS);
        juego = savedInstanceState.getString(Constantes.JUEGO);
        norma = getString(R.string.norma);
    }

    /**
     * Obtiene una prueba de la lista de pruebas.
     *
     * @return Prueba
     */
    private String Obtener_Prueba()
    {
        Random random = new Random();
        int index = random.nextInt(lPruebas.size());
        return lPruebas.get(index);
    }

    /**
     * Envia la norma al fragment del juego
     *
     * @param REQUEST_CODE Código de la petición para poder enviar el string.
     */
    private void sendResult(int REQUEST_CODE)
    {
        Intent intent = new Intent();
        intent.putExtra(Constantes.NORMA, prueba);
        getTargetFragment().onActivityResult(getTargetRequestCode(), REQUEST_CODE, intent);
    }

    //endregion
}


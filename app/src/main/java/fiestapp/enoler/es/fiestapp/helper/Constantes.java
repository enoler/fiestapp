package fiestapp.enoler.es.fiestapp.helper;

/**
 * Constantes utilizadas en la aplicación.
 */
public class Constantes
{
    public static final String FRAGMENT = "fragment";
    public static final String SR3 = "Señor del 3";

    public static final String FORMATEAR_ENTERO = "%d";

    public static final String LISTA         = "Lista de...";
    public static final String YO_NUNCA      = "Yo nunca...";
    public static final String PRUEBAS       = "PRUEBAS";
    public static final String NORMA         = "Norma";
    public static final String JUEGO         = "JUEGO";
    public static final String ARRAY_JUGADORES        = "mJugadores";

    public static final String ERROR_DADO      = "Error en dado";
    public static final String ERROR_JUGADORES = "Error jugadores";
}

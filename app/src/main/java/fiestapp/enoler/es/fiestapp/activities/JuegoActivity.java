package fiestapp.enoler.es.fiestapp.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import fiestapp.enoler.es.fiestapp.R;
import fiestapp.enoler.es.fiestapp.fragments.Dado.JugadoresFragment;
import fiestapp.enoler.es.fiestapp.fragments.YoNunca.YoNuncaFragment;
import fiestapp.enoler.es.fiestapp.helper.Constantes;

/**
 * Actividad correspondiente al juego.
 */
public class JuegoActivity extends AppCompatActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_juego);

        String juego = getIntent().getStringExtra(Constantes.FRAGMENT);

        Seleccionar_Juego(juego);
    }

    private void Seleccionar_Juego(String juego)
    {
        if(findViewById(R.id.rl_fragment) != null)
        {
            switch(juego)
            {
                case Constantes.SR3:
                    getSupportFragmentManager().beginTransaction()
                            .add(R.id.rl_fragment, new JugadoresFragment()).commit();
                    break;
                case Constantes.YO_NUNCA:
                    getSupportFragmentManager().beginTransaction()
                            .add(R.id.rl_fragment, new YoNuncaFragment()).commit();
                    break;
            }
        }
    }
}

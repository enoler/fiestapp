package fiestapp.enoler.es.fiestapp.fragments.YoNunca;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.Button;
import android.widget.TextView;
import fiestapp.enoler.es.fiestapp.R;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 * Fragment del juego Yo Nunca.
 */
public class YoNuncaFragment extends Fragment
{
    private List<String> mPruebas;

    public YoNuncaFragment() { }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflar el fragment.
        View v = inflater.inflate(R.layout.fragment_yonunca_fragment, container, false);

        // Inicializar.
        final TextView tv_yonunca = (TextView) v.findViewById(R.id.tv_yonunca);
        Button bt_otro = (Button) v.findViewById(R.id.bt_yonunca);
        mPruebas = Arrays.asList(getResources().getStringArray(R.array.yo_nunca_array_suaves));

        // Seleccionar un Yo Nunca Random inicial.
        Random r = new Random();
        int index = r.nextInt(mPruebas.size() - 1);
        tv_yonunca.setText(mPruebas.get(index));

        // Manejador del click del botón.
        bt_otro.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                Random r = new Random();
                int index = r.nextInt(mPruebas.size() - 1);
                tv_yonunca.setText(mPruebas.get(index));
            }
        });

        return v;
    }

}

package fiestapp.enoler.es.fiestapp.fragments.Dado;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.res.ResourcesCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.*;

import fiestapp.enoler.es.fiestapp.R;
import fiestapp.enoler.es.fiestapp.components.DialogDadoPrueba;
import fiestapp.enoler.es.fiestapp.helper.Constantes;
import fiestapp.enoler.es.fiestapp.models.Jugador;
import fiestapp.enoler.es.fiestapp.models.Dado;
import java.util.*;

/**
 * Fragment principal de la partida.
 */
public class DadoFragment extends Fragment
{
    //region Constantes

    private static final Integer REQUEST_CODE = 0;

    //endregion

    //region Variables globales

    private TextView mNormaActual;
    private TextView tv_jugador;
    private TextView tv_accion;
    private TextView tv_sr_3;
    private ImageView iv_dado;
    private ImageView iv_jugador;
    private FragmentManager fm;
    private String sres_3;

    private Dictionary<Integer, String> dJuegos;
    private ArrayList<Jugador> mJugadores;
    private List<String> lYoNuncas;
    private List<String> lNormas;
    private List<String> lListas;
    private int contador_jugador;
    private Handler mHandler;
    private Dado dado;

    //endregion

    //region Constructores

    public DadoFragment() { }

    public static DadoFragment newInstance(ArrayList<Jugador> jugadores)
    {
        DadoFragment fragment = new DadoFragment();

        Bundle args = new Bundle();
        args.putParcelableArrayList(Constantes.ARRAY_JUGADORES, jugadores);
        fragment.setArguments(args);

        return fragment;
    }

    //endregion

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflar el layout.
        View v = inflater.inflate(R.layout.fragment_dado_juego, container, false);

        // Asignar vistas a variables.
        Button bt_tirar = (Button) v.findViewById(R.id.bt_dado);
        mNormaActual    = (TextView) v.findViewById(R.id.tv_norma_actual);
        tv_jugador      = (TextView) v.findViewById(R.id.tv_jugador);
        tv_accion       = (TextView) v.findViewById(R.id.tv_prueba);
        tv_sr_3         = (TextView) v.findViewById(R.id.tv_sr_3_actual);
        iv_dado         = (ImageView) v.findViewById(R.id.iv_dado);
        iv_jugador        = (ImageView) v.findViewById(R.id.imagen_jugador);
        // Se inicializan las variables globales.
        Inicializar();

        // Manejador del botón tirar.
        bt_tirar.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                Tirar_Dado();
            }
        });

        return v;
    }

    //region Funciones

    /**
     * Se cambia el jugador que le toca.
     */
    private void Cambiar_Jugador()
    {
        // Se cambia el turno al jugador siguiente.
        if(contador_jugador < mJugadores.size() - 1)
            contador_jugador++;
        else
            contador_jugador = 0;
        tv_jugador.setText(mJugadores.get(contador_jugador).getName());
        iv_jugador.setImageResource(mJugadores.get(contador_jugador).getProfilePhotoLocation());
    }

    /**
     * Cambia el señor del tres en el textview y en el arraylist.
     */
    private void Cambiar_Sr_3(int index)
    {
        // Obtener nombre actual.
        String nombre_nuevo  = mJugadores.get(index).getName();
        String nombre_actual = tv_jugador.getText().toString();

        // Cambiar en textview.
        tv_sr_3.setText(tv_sr_3.getText().toString().replace(nombre_actual, nombre_nuevo));

        // Cambiar en el array.
        mJugadores.get(contador_jugador).setSr_3(false);
        mJugadores.get(index).setSr_3(true);

        Cambiar_Jugador();
    }

    /**
     * Si hay jugadores se muestra el textview de jugador y del señor del tres.
     */
    private void Cambiar_Visibilidad()
    {
        // Si hay jugadores se ponen visibles.
        if(mJugadores.size() != 0)
        {
            tv_jugador.setVisibility(View.VISIBLE);
            tv_sr_3.setVisibility(View.VISIBLE);
            // Se pone al primer jugador.
            contador_jugador = 0;
            tv_jugador.setText(mJugadores.get(contador_jugador).getName());
            iv_jugador.setImageResource(mJugadores.get(contador_jugador).getProfilePhotoLocation());
        }
        else
        {
            tv_jugador.setVisibility(View.GONE);
            tv_sr_3.setVisibility(View.GONE);
        }
    }

    /**
     * Se cargan las pruebas de los ficheros "string" a su List correspondiente.
     */
    private void Cargar_Datos()
    {
        // Se obtienen las pruebas.
        lYoNuncas  = Arrays.asList(getResources().getStringArray(R.array.yo_nunca_array_suaves));
        lListas    = Arrays.asList(getResources().getStringArray(R.array.listas_array));
        lNormas    = Arrays.asList(getResources().getStringArray(R.array.normas_array));

        sres_3 = getResources().getString(R.string.sres_3) + ": \n";

        // Se obtienen los jugadores.
        try
        {
            Bundle extras = getArguments();
            mJugadores = extras.getParcelableArrayList(Constantes.ARRAY_JUGADORES);
        }
        catch (Exception e)
        {
            Log.e(Constantes.ERROR_JUGADORES, e.toString());
            mJugadores = new ArrayList<>();
        }
    }

    /**
     * Se crea el dialog, se añade un bundle con el nombre del juego y sus pruebas y se muestra.
     */
    private void Dialog_Prueba()
    {
        DialogDadoPrueba dialog = new DialogDadoPrueba();
        Bundle bundle           = new Bundle();

        bundle.putString(Constantes.JUEGO, dado.getJuego());
        bundle.putStringArrayList(Constantes.PRUEBAS, Seleccionar_Juego(dado.getJuego()));
        dialog.setArguments(bundle);
        dialog.setTargetFragment(this, REQUEST_CODE);
        dialog.setCancelable(false);
        dialog.show(fm, dado.getJuego());
    }

    /**
     * Se abre un dialog para seleccionar un nuevo señor del 3.
     */
    private void Dialog_Sr_3()
    {
        // Crear builder.
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final View dialogView = View.inflate(getContext(), R.layout.dialog_dado_sr3, null);
        builder.setView(dialogView);
        final RadioGroup rg = (RadioGroup) dialogView.findViewById(R.id.rg_nuevo_sr3);

        // Añadir jugadores al radio group.
        for(Jugador j : mJugadores)
        {
            RadioButton rb = new RadioButton(getActivity());
            rb.setText(j.getName());
            rg.addView(rb);
        }

        // Seleccionar uno aleatorio para poner por defecto.
        Random r = new Random();
        int index = r.nextInt(mJugadores.size() - 1);
        rg.check(index);

        builder.setTitle(getActivity().getResources().getString(R.string.cambiar_sr3))
                .setMessage(getActivity().getResources().getString(R.string.elegir_sr3))
                .setPositiveButton(getActivity().getResources().getString(R.string.aceptar), new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int id)
                    {
                        Cambiar_Sr_3(rg.getCheckedRadioButtonId());
                    }
                });

        // Se crea el Dialog y se retorna.
        AlertDialog b = builder.create();
        b.setCancelable(false);
        b.show();
    }

    /**
     * Se inicializan las variables globales.
     */
    private void Inicializar()
    {
        mHandler = new Handler();
        Rellenar_Diccionario();   // Se relacionan los números del dado con cada juego.
        Cargar_Datos();           // Se obtienen las pruebas de cada juego.
        Cambiar_Visibilidad();    // Se cambia la visibilidad de algunos textview.
        Mostrar_Sres_3();         // Se muestran los señores del 3.

        dado = new Dado(dJuegos); // Se inicializa el dado.
        fm = getFragmentManager();
    }

    /**
     * Se pone en el textview los actuales señores del 3.
     */
    private void Mostrar_Sres_3()
    {
        for(Jugador j : mJugadores)
        {
            if(j.isSr_3())
                sres_3 += j.getName() + "\n";
        }
        tv_sr_3.setText(sres_3);
    }

    /**
     * Se rellena el diccionario asociando un número de dado a un juego.
     */
    private void Rellenar_Diccionario()
    {
        dJuegos = new Hashtable<>();
        dJuegos.put(1, getResources().getText(R.string.beber).toString());
        dJuegos.put(2, getResources().getText(R.string.mandar_beber).toString());
        dJuegos.put(3, getResources().getText(R.string.bebe_sr_3).toString());
        dJuegos.put(4, getResources().getText(R.string.yo_nunca).toString());
        dJuegos.put(5, getResources().getText(R.string.lista).toString());
        dJuegos.put(6, getResources().getText(R.string.norma).toString());
    }

    /**
     * Se obtiene una lista de pruebas en función del juego que haya salido en el dado.
     *
     * @param juego Nombre del juego que ha salido en el dado.
     * @return Lista de pruebas correspondientes a su juego.
     */

    private ArrayList<String> Seleccionar_Juego(String juego)
    {
        switch (juego)
        {
            case Constantes.LISTA:
                return new ArrayList<>(lListas);
            case Constantes.YO_NUNCA:
                return new ArrayList<>(lYoNuncas);
            case Constantes.NORMA:
                return new ArrayList<>(lNormas);
            default:
                break;
        }

        return null;
    }

    /**
     * Se tira el dado y se ejecuta la acción correspondiente.
     */
    private void Tirar_Dado()
    {
        // Se tira el dado.
        new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                long startTime = System.currentTimeMillis();
                while ((System.currentTimeMillis() - startTime) < 800)
                {
                    try
                    {
                        Thread.sleep(100);
                        mHandler.post(new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                dado.Tirar();

                                // Poner imagen según el número obtenido.
                                Drawable dado_drawable;
                                switch(dado.getNumero())
                                {
                                    case 1:
                                        dado_drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.d1, null);
                                        break;
                                    case 2:
                                        dado_drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.d2, null);
                                        break;
                                    case 3:
                                        dado_drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.d3, null);
                                        break;
                                    case 4:
                                        dado_drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.d4, null);
                                        break;
                                    case 5:
                                        dado_drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.d5, null);
                                        break;
                                    case 6:
                                        dado_drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.d6, null);
                                        break;
                                    default:
                                        dado_drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.d1, null);
                                        break;
                                }
                                iv_dado.setImageDrawable(dado_drawable);
                            }
                        });
                    }
                    catch (Exception e)
                    {
                        Log.e(Constantes.ERROR_DADO, e.toString());
                    }
                }
            }
        }).start();

        // Se hace la acción correspondiente.
        mHandler.postDelayed(new Runnable()
        {
            @Override
            public void run()
            {
                tv_accion.setText(dado.getJuego());

                // Muestra el dialog correspondiente si sale 4, 5 o 6.
                if (dado.getNumero() >= 4)
                    Dialog_Prueba();
                // Si sale un 3 y el jugador que tiró es señor del 3.
                else if(dado.getNumero() >= 3
                        && tv_sr_3.getText().toString().contains(tv_jugador.getText().toString())
                        && mJugadores.size() != 0)
                {
                    Dialog_Sr_3();
                }
                else if(mJugadores.size() != 0)
                    Cambiar_Jugador();
            }
        }, 1000);
    }

    /**
     * Recibe la norma del dialog
     * @param requestCode Código de la petición.
     * @param resultCode Código resultante.
     * @param data Datos que se pasan.
     */
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        // Si el request code coincide, recuperamos el string.
        if (resultCode == REQUEST_CODE)
        {
            String norma = getResources().getString(R.string.norma) + ": "
                    + data.getStringExtra(Constantes.NORMA);
            mNormaActual.setText(norma);
        }

        // Se cambia de jugador.
        if(mJugadores.size() != 0)
            Cambiar_Jugador();
    }

    //endregion
}

package fiestapp.enoler.es.fiestapp.models;

import java.util.Dictionary;
import java.util.Random;

/**
 * Modelo de datos de la clase Dado.
 */
public class Dado
{
    //region Atributos.

    private Dictionary<Integer, String> Numero_Accion;
    private String Juego;
    private int Numero;

    //endregion

    //region Getters

    public String getJuego()
    {
        return Juego;
    }

    public int getNumero()
    {
        return Numero;
    }

    //endregion

    public Dado(Dictionary<Integer, String> Numero_Accion)
    {
        this.Numero_Accion = Numero_Accion;
    }

    /**
     * Se tira el dado y selecciona un número entre 1 y 6.
     */
    public void Tirar()
    {
        Random num = new Random();
        Numero = num.nextInt(6) + 1;
        Juego = Numero_Accion.get(Numero);
    }
}

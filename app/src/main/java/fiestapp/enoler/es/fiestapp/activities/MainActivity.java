package fiestapp.enoler.es.fiestapp.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import fiestapp.enoler.es.fiestapp.R;

/**
 * Actividad principal del menú.
 * Contiene Fragment añadido por XML.
 */
public class MainActivity extends AppCompatActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}

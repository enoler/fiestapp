package fiestapp.enoler.es.fiestapp.models;

import android.graphics.drawable.Drawable;

/**
 * Clase padre asociada a todos los elementos que se van a mostrar en un gridview en la sección Introducir Datos.
 */
public class ElementoGrid
{
    //region Grid

    private int      Id;
    private String   Nombre;
    private int imagen;

    //endregion

    //region Getters & setters

    public int getId()
    {
        return Id;
    }

    public void setId(int id)
    {
        Id = id;
    }

    public String getNombre()
    {
        return Nombre;
    }

    public void setNombre(String nombre)
    {
        Nombre = nombre;
    }

    public int getImagen()
    {
        return imagen;
    }

    public void setImagen(int imagen)
    {
        this.imagen = imagen;
    }

    //endregion
}

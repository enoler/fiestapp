package fiestapp.enoler.es.fiestapp.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import fiestapp.enoler.es.fiestapp.R;
import fiestapp.enoler.es.fiestapp.components.JugadorHolder;
import fiestapp.enoler.es.fiestapp.models.Jugador;

public class JugadorAdapter extends RecyclerView.Adapter<JugadorHolder> {

    private List<Jugador> jugadorList;

    public JugadorAdapter(List<Jugador> jugadorList) {
        this.jugadorList = jugadorList;
    }

    @Override
    public JugadorHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.jugador_layout, parent, false);

        return new JugadorHolder(itemView);
    }

    @Override
    public void onBindViewHolder(JugadorHolder holder, int position) {
        Jugador item = jugadorList.get(position);
        holder.txtCelebName.setText(item.getName());
        holder.profileImage.setImageResource(item.getProfilePhotoLocation());

    }

    @Override
    public int getItemCount() {
        return jugadorList.size();
    }
}

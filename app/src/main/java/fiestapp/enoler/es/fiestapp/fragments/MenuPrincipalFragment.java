package fiestapp.enoler.es.fiestapp.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import fiestapp.enoler.es.fiestapp.R;
import fiestapp.enoler.es.fiestapp.adapters.GridAdapter;
import fiestapp.enoler.es.fiestapp.helper.Constantes;
import fiestapp.enoler.es.fiestapp.models.ElementoGrid;

import static fiestapp.enoler.es.fiestapp.helper.Constantes.SR3;

/**
 * Pantalla principal.
 */
public class MenuPrincipalFragment extends Fragment
{
    private ArrayList<ElementoGrid> mJuegos;
    private RecyclerView recyclerView;
    private GridAdapter adapter;
    private Context context;
    // Constructor por defecto.
    public MenuPrincipalFragment() { }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflar el layout.
        View view = inflater.inflate(R.layout.fragment_grid_principal, container, false);
        mJuegos=  new ArrayList<>();
        context = view.getContext();
        Inicializar_Juegos();
        recyclerView = (RecyclerView)view.findViewById(R.id.recycler_view_principal);
        adapter = new GridAdapter(context,mJuegos);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);


        return view;
    }

    /**
     * Inicializa los juegos disponibles.
     */
    private void Inicializar_Juegos()
    {
        Meter_Juego(SR3, R.drawable.senor3);      // Sr. del 3
        Meter_Juego(Constantes.YO_NUNCA, R.drawable.yonunca);  // Yo nunca
    }

    /**
     * Se introduce un juego en el array del gridview.
     * @param nombre Nombre del juego.
     * @param imagen Imagen que se mostrará.
     */
    private void Meter_Juego(String nombre, int imagen)
    {
        ElementoGrid juego = new ElementoGrid();
        juego.setNombre(nombre);
        juego.setImagen(imagen);
        mJuegos.add(juego);
    }
}
